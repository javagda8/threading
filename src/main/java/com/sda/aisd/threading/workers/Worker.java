package com.sda.aisd.threading.workers;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Worker implements Runnable {
    private final ExecutorService watek = Executors.newSingleThreadExecutor();

    private static final int TIME_SLEEP_WKLAD = 1000;
    private static final int TIME_SLEEP_OBUDOWA = 3000;
    private static final int TIME_SLEEP_SKRECENIE = 4000;
    private static final int TIME_SLEEP_PRZERWA = 15000;

    private String name;
    private boolean isWorking;

    private IWorkplace workplace;

    public Worker(IWorkplace workplace, String name) {
        this.workplace = workplace;
        this.name = name;

        watek.submit(this);
    }

    @Override
    public void run() {
        isWorking = true;

        int penCounter = 0;
        while (isWorking) {
            penCounter++;
            try {
                Thread.sleep(TIME_SLEEP_WKLAD);
                System.out.println("Wklad stworzony przez " + name);
                Thread.sleep(TIME_SLEEP_OBUDOWA + generateRandomDelay());
                System.out.println("Obudowa stworzona przez " + name);
                Thread.sleep(TIME_SLEEP_SKRECENIE - generateRandomDelay());
                System.out.println("Dlugopis skrecony przez" + name);

                workplace.addDlugopis(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (penCounter % 15 == 0) {
                try {
                    System.out.println(name + " rozpoczyna przerwę");
                    Thread.sleep(TIME_SLEEP_PRZERWA);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void stopWorker() {
        isWorking = false;
    }

    private int generateRandomDelay() {
        Random random = new Random();
        return random.nextInt(1000);
    }
}

package com.sda.aisd.threading.workers;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Workplace workplace = new Workplace();

        Scanner sc = new Scanner(System.in);

        boolean isWorking = true;
        while (isWorking){
            String line = sc.nextLine();

            if(line.startsWith("add ")){
                workplace.addWorker(line.replace("add ", ""));
            }else if(line.equals("quit")){
                break;
            }
        }
    }
}

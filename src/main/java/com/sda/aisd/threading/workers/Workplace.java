package com.sda.aisd.threading.workers;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Workplace implements IWorkplace{
    //    private ExecutorService pulaWatkow = Executors.newFixedThreadPool(5);
    private List<Worker> workers = new LinkedList<>();

    private int iloscDlugopisow = 0;

    private final Object lock = new Object();

    public Workplace() {
        for (int i = 0; i < 5; i++) {
            Worker worker = new Worker(this, "" + i);
//            pulaWatkow.submit(worker);
            workers.add(worker);
        }
    }

    public void addWorker(String name) {
        Worker worker = new Worker(this, name);
        workers.add(worker);
    }

    public void stopProducing() {
        for (Worker worker : workers) {
            worker.stopWorker();
        }
    }

    public int getIloscDlugopisow() {
        return iloscDlugopisow;
    }

    @Override
    public void addDlugopis(int ile) {
        synchronized (lock) {
            iloscDlugopisow += ile;
            System.out.println("Dodaję długopis, mam już: " + iloscDlugopisow + " dlugopisow.");
        }
    }
}

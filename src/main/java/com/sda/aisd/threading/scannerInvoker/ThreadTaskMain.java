package com.sda.aisd.threading.scannerInvoker;

import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadTaskMain {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        ExecutorService service = Executors.newCachedThreadPool();

        boolean isWorking = true;
        while (isWorking){
            String line = sc.nextLine();

            if(!line.isEmpty()) {
                String[] slowa = line.split(" ");

                Integer ileSpac = Integer.parseInt(slowa[0]);

                ThreadTask task = new ThreadTask(ileSpac);
                service.submit(task);
                if (line.equals("quit")) {
                    break;
                }
            }else{
                System.out.println("Brak parametrów");
            }
        }
    }
}

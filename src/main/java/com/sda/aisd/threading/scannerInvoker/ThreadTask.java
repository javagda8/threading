package com.sda.aisd.threading.scannerInvoker;

public class ThreadTask implements Runnable {
    private static int counter = 0;

    private int ileSpac;

    public ThreadTask(int ileSpac) {
        this.ileSpac = ileSpac;
    }

    public void run() {
        Thread.currentThread().setName("" + counter++);
        System.out.println("Rozpoczynam");
        try {
            Thread.sleep(ileSpac);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Zakończyłem");
    }
}

package com.sda.aisd.threading.examples;

public class FileTaskExample {
    public static void main(String[] args) {
//        File f = new File("/home/amen/Documents/");
//        for (File subFile: f.listFiles()) {
//            if(subFile.getName().equals("jakistamplik")){
//                System.out.println("Znalazłem!");
//            }
//        }
        FileCheckTask task = new FileCheckTask("/home/amen/Documents");

        Thread th = new Thread(task);
        // start
        th.start();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        task.setCoPisac("Cokolwiek byle nie null");
        th.interrupt();
        // zatrzymanie
//        task.stopThread();
    }
}

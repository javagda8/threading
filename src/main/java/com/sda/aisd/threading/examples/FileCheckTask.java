package com.sda.aisd.threading.examples;

import java.io.File;

public class FileCheckTask implements Runnable {
    private String path;
    private boolean isWorking;
    private String coPisac = null;

    public FileCheckTask(String path) {
        this.path = path;
    }

    public void run() {
        isWorking = true;

        try {
            while (isWorking) {
                System.out.println(coPisac);
                Thread.sleep(5000);
                File f = new File(path);
                for (File subFile : f.listFiles()) {
                    if (subFile.getName().equals("jakistamplik")) {
                        System.out.println("Znalazłem!");
                    }
                }
            }
        } catch (InterruptedException ie) {
            System.out.println("Interrupted");
//                break;
        }
//

        // sprzątanie
    }

    public void setCoPisac(String coPisac) {
        this.coPisac = coPisac;
    }

    public void stopThread() {
        isWorking = false;
    }

}

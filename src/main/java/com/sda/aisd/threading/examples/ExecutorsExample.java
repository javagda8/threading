package com.sda.aisd.threading.examples;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecutorsExample {
    public static void main(String[] args) {
        // Executors
        // ExecutorService

        ExecutorService service = Executors.newCachedThreadPool();
        service.submit(new FileCheckTask("/home/amen/Documents"));

    }
}

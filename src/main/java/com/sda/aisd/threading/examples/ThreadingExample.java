package com.sda.aisd.threading.examples;

public class ThreadingExample {
    public static void main(String[] args) {
        // Run - uruchomić
        // Runnable - uruchamialny
        Runnable obiektWykonywalny = new Runnable() {
            public void run() {
                System.out.println("Hello");
            }
        };
        Thread watek = new Thread(obiektWykonywalny);
        watek.start();


        for (int i = 0; i < 10; i++) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

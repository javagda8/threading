package com.sda.aisd.threading.banking;

public class BankAccount {
    private double balance = 0.0;
    private final Object lock = new Object();

    public synchronized void add(double howMuch) {
        synchronized (lock) {
            balance = balance + howMuch;
        }
    }

    public synchronized void sub(double howMuch) {
        synchronized (lock) {
            balance = balance - howMuch;
        }
    }

    public void balance() {
        System.out.println(balance);
    }
}

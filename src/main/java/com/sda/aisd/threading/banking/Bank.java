package com.sda.aisd.threading.banking;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Bank {
    private BankAccount account = new BankAccount();
    private ExecutorService serwery = Executors.newFixedThreadPool(5);

    private List<ZleceniePrzelewu> zlecenia = new LinkedList<>();


    public void subtractFromAccount(double howMuch){
        // zlecenie przelewu
        ZleceniePrzelewu zleceniePrzelewu = new ZleceniePrzelewu(account, howMuch, KierunekPrzelewu.SUB);
        serwery.submit(zleceniePrzelewu);
    }

    public void addToAccount(double howMuch){
        // zlecenie przelewu
        ZleceniePrzelewu zleceniePrzelewu = new ZleceniePrzelewu(account, howMuch, KierunekPrzelewu.ADD);
        serwery.submit(zleceniePrzelewu);

        zlecenia.add(zleceniePrzelewu);
    }

    public void balanceAccount(){
        account.balance();
    }

    public void shutdown(){
        serwery.shutdown();
    }
}
